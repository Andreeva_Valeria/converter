package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

/**
 * @author Andreeva V.A.
 */
@SuppressWarnings("unused")
public class Controller {

    @FXML
    private TextField inputNumberField;

    @FXML
    private ComboBox<?> convertFrom;

    @FXML
    private ComboBox<?> convertTo;

    @FXML
    private TextField resultOfConvert;

    @FXML
    void clear(ActionEvent event) {
        getAnimation(inputNumberField);
        getAnimation(resultOfConvert);
        inputNumberField.clear();
        resultOfConvert.clear();
    }

    /**
     * Позволяет осуществить и Выводит результат конвертации в приложения
     */
    @FXML
    void convert(ActionEvent event) {
        double number = getNumber(inputNumberField);
        if (number <= 0) {
            getAnimation(inputNumberField);
            resultOfConvert.setText("Некорректные данные");
            return;
        }
        double result = 0;
        isEmpty(inputNumberField);

        int choice = choiceValue();
        if (choice == 1 || choice == 2) {
            result = fromRUB(number, choice);
        }
        if (choice == 3 || choice == 4) {
            result = fromUSD(number, choice);
        }
        if (choice == 5 || choice == 6) {
            result = fromEUR(number, choice);
        }
        if (choice == 7) {
            choiceBox();
            return;
        }
        if (choice == -1 || choice == 0) {
            resultOfConvert.setText("Выберите единицы измерения");
            return;
        }
        resultOfConvert.setText(String.valueOf(result));
    }

    /**
     * Выводит соответствующее сообщение о том, что выбрана одна и та же валюта
     */
    private void choiceBox() {
        getAnimation(inputNumberField);
        getAnimation(resultOfConvert);
        resultOfConvert.setText("Выбрана одна и та же валюта");
    }

    /**
     * Переводит евро в доллары или рубли
     *
     * @param number введеное число
     * @param choice выбор перевода (5 - в рубли, 6 - в доллары)
     * @return результат перевода
     */
    private double fromEUR(double number, int choice) {
        if (choice == 5)
            return number * 79.86;
        else return number * 1.08;
    }

    /**
     * Переводит доллары в евро или рубли
     *
     * @param number введеное число
     * @param choice выбор перевода (3 - в рубли, 4 - в евро)
     * @return результат перевода
     */
    private double fromUSD(double number, int choice) {
        if (choice == 3)
            return number * 74.14;
        else return number / 1.08;
    }

    /**
     * Переводит рубли в доллары или евро
     *
     * @param number введеное число
     * @param choice выбор перевода (1 - в доллары, 2 - в евро)
     * @return результат перевода
     */
    private double fromRUB(double number, int choice) {
        if (choice == 1)
            return number / 74.14;
        else return number / 79.86;
    }

    /**
     * Позволяет получить данные из Комбобоксов для перевода валют
     *
     * @return число, в зависимости от выбора, если не выбрано ничего возвращает -1
     */
    @FXML
    private int choiceValue() {
        try {
            if (convertFrom.getValue().equals("RUB") && convertTo.getValue().equals("USD")) {
                return 1;
            } else if (convertFrom.getValue().equals("RUB") && convertTo.getValue().equals("EUR")) {
                return 2;
            } else if (convertFrom.getValue().equals("USD") && convertTo.getValue().equals("RUB")) {
                return 3;
            } else if (convertFrom.getValue().equals("USD") && convertTo.getValue().equals("EUR")) {
                return 4;
            } else if (convertFrom.getValue().equals("EUR") && convertTo.getValue().equals("RUB")) {
                return 5;
            } else if (convertFrom.getValue().equals("EUR") && convertTo.getValue().equals("USD")) {
                return 6;
            } else if (convertFrom.getValue().equals("RUB") && convertTo.getValue().equals("RUB")) {
                return 7;
            } else if (convertFrom.getValue().equals("EUR") && convertTo.getValue().equals("EUR")) {
                return 7;
            } else if (convertFrom.getValue().equals("USD") && convertTo.getValue().equals("USD")) {
                return 7;
            }
        } catch (Exception e) {
            return -1;
        }
        return 0;
    }

    /**
     * Проверяет наполненность поля для ввода числа
     *
     * @param number поле ввода числа
     */
    private void isEmpty(TextField number) {
        if (number.getText().isEmpty()) {
            getAnimation(number);
        }
    }

    /**
     * Выполняет анимацию полей последством класса Shake
     *
     * @param number поле ввода данных
     */
    private void getAnimation(TextField number) {
        Shake animNumber = new Shake(number);
        animNumber.playAnimation();
    }

    /**
     * Осуществляет проверку на валидность введеный данные, если все ок - возращает вещ.значение, иначе сообщает об ошибке с последующей анимацией полей
     *
     * @param operand операнд - текстовое поле
     * @return вещ.значение
     */
    private double getNumber(TextField operand) {
        try {
            return Double.parseDouble(operand.getText());
        } catch (Exception e) {
            inputNumberField.clear();
            getAnimation(inputNumberField);
            getAnimation(resultOfConvert);
            resultOfConvert.setText("Ошибка ввода данных");
        }
        return 0;
    }
}
